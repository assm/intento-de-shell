#ifndef MACROS_H
#define MACROS_H

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <termios.h>
#include <boost/algorithm/string/replace.hpp>

#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " \t\r\n\a"

using namespace std;
using namespace boost;

#endif
