#ifndef SHELL_H
#define SHELL_H
#include "./builtins.hpp"
#include "../structures/trie.hpp"

class Shell
{
private:
  Trie savedata;
  ofstream history;
  char **vstochar(vector<string> args)
  {
    char** data = (char**)malloc(sizeof(char*) * args.size());
    if (!data) {
      fprintf(stderr, "sh: allocation error\n");
      exit(EXIT_FAILURE);
    }
    for(int i = 0; i < args.size(); i++)
    {
      char* temp = (char*)args[i].c_str();
      data[i] = temp;
    }
    return data;
  }
public:
  Shell()
  {
    string route = string(getenv("HOME")) + "/.someshell";
    ifstream data(route);
    string command;
    while(data >> command)
    {
      savedata.insert(command);
    }
    data.close();
    history.open(route, ios::app);
  }
  ~Shell()
  {
    history.close();

  }
  void savecommand(string command)
  {
    savedata.insert(command);
    history << command << endl;
  }
  void mainloop()
  {
    string line = "";
    vector<string> args;
    char **argss;
    int status;
    do {
      do {
        printhead();
        line = savedata.readline(printhead);
      } while(!line.length());
      replace_all(line, "~", string(getenv("HOME")));
      args = split_line(line);
      argss = vstochar(args);
      status = execute(argss);
      savecommand(line);
      
      free(argss);
    } while(status);
  }

  vector<string> split_line(string sline)
  {
    char* line = (char*)sline.c_str();
    char* token;
    vector<string> tokens;

    token = strtok(line, LSH_TOK_DELIM);
    while(token != NULL)
    {
      string temp = token;
      tokens.push_back(temp);
      token = strtok(NULL, LSH_TOK_DELIM);
    }
    return tokens;
  }

  int launch(char** args)
  {
    pid_t pid, wpid;
    int status;
    
    pid = fork();
    if(pid == 0)
    {
      if(execvp(args[0], args) == -1)
      {
        perror("sh");
      }
      exit(EXIT_FAILURE);
    }
    else if (pid < 0)
    {
      perror("sh");
    }
    else
    {
      do {
        wpid = waitpid(pid, &status, WUNTRACED);
      } while(!WIFEXITED(status) && !WIFSIGNALED(status));
    }
    return 1;
  }
  int execute(char** args)
  {
    int i;
    if (args[0] == NULL) {
      return 1;
    }
    for (i = 0; i < sh_num_builtins(); i++) {
      if (strcmp(args[0], builtin_str[i].c_str()) == 0) {
        return (*builtin_func[i])(args);
      }
    }
    return launch(args);
  }

  static void printhead()
  {
    char cwd[200];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
      string cwd2 = cwd;
      replace_all(cwd2, string(getenv("HOME")), "~");
      printf("%s", cwd2.c_str());
    }else{
      perror("getcwd() error");
      return;
    }
    printf(" > ");
  }
};

#endif
