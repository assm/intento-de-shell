#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include "./macros.hpp"

vector<string> getfiles()
{
  vector<string> names;
  DIR *d;
  struct dirent *dir;
  d = opendir(".");
  if(d)
  {
    while((dir = readdir(d)) != NULL)
    {
      string temp = dir->d_name;
      names.push_back(temp);
    }
    closedir(d);
  }
  return names;
}

#endif
