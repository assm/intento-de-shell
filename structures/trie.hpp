#ifndef TRIE_H
#define TRIE_H
#include "../headers/macros.hpp"

struct Node {
  bool isNode;
  map<char, Node*> children;
  Node(): isNode(false) {}
  ~Node()
  {
    for(auto itr = children.begin(); itr != children.end(); itr++)
    {
      if(itr->second)
        delete itr->second;
    }
  }
  bool insert(int position, string value)
  {
    if(position == value.length())
    {
      if(isNode){return false;}
      isNode = true;
      return true;
    }
    Node** temp = &(children[value[position]]);
    if(!(*temp))
    {
      *temp = new Node;
    }
    return (*temp)->insert(position+1, value);
  }
  Node* search(string word, int position)
  {
    if(position == word.length())
    {
      return this;
    }
    Node **temp = &(children[word[position]]);
    if(!(*temp))
    {
      return nullptr;
    }
    return (*temp)->search(word, position+1);
  }
  void delet(string value, int position)
  {
    if(position == value.length())
    {
      return;
    }
    Node** temp = &(children[value[position]]);
    if(!(*temp))
    {
      return;
    }
    (*temp)->delet(value, position+1);
    if((*temp)->children.empty())
    {
      delete *temp;
      *temp = nullptr;
    }
    else
    {
      (*temp)->isNode = false;
    }
  }
  void getfirst(string &word)
  {
    if(!children.size()){return;}
    if(children.begin()->second)
    {
      word.push_back(children.begin()->first);
      children.begin()->second->getfirst(word);
    }
  }
  friend class Trie;
};

class Trie {
private:
  Node *root;
  Node* search(string word)
  {
    if(!root){return nullptr;}
    return root->search(word, 0);
  }
public:
  Trie(): root(nullptr) {}
  ~Trie()
  {
    delete root;
  }
  void insert(string val)
  {
    if(!root)
    {
      root = new Node;
    }
    root->insert(0, val);
  }
  void delet(string word)
  {
    if(!root){return;}
    root->delet(word, 0);
  }

  string getword(string text)
  {
    Node* svalue = search(text);
    string val = "";
    if(svalue){svalue->getfirst(val);}
    return text+val;
  }

  void complete(string text)
  {
    Node* svalue = search(text);
    string val = "";
    if(svalue){svalue->getfirst(val);}
    cout << "\033[34m" << val << "\033[0m";
    printf("%c[0K", 27);
  }

  string readline(void (*head)())
  {
    string value = "";
    string completeword = "";
    int c = ' ';
    static struct termios oldt, newt;
    tcgetattr( STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON);
    tcsetattr( STDIN_FILENO, TCSANOW, &newt);
    do {
      c = getchar();
      if(c == 27)
      {
        getchar();
        c = getchar();
        switch(c)
        {
          case 'A':
            //cout << "UP";
            break;
          case 'B':
            //cout << "DOWN";
            break;
          case 'C':
            //cout << "RIGHT";
            break;
          case 'D':
            //cout << "LEFT";
            break;
        }
        cout << endl;
        break;
      }
      if(c != '\n' && c != EOF)
      {
        if(c != '\t')
        {
          if(c == 127)
          {
            if(value.length() > 0)
            {
              value.pop_back();
              if(value.length() > 0)
                complete(value);
            }
            printf("%c[1K", 27);
          }
          else
          {
            value.push_back(c);
            complete(value);
          }
          cout << "\r" << flush;
          head();
          cout << value;
        }
        else
        {
          if(value.length() > 0)
          {
            value = getword(value);
          }
          cout << "\r" << flush;
          head();
          cout << value;
        }
      }
    } while(c != '\n' && c != EOF);
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt);
    return value;
  }
};


#endif
